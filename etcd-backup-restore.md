# Implement etcd backup and restore

[Implement etcd backup and restore](https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster)

## etcd backup

### Backup a single instance of etcd
<details><summary>show</summary>
<p>

```
# Check info
kubectl -n kube-system get pods
kubectl -n kube-system describe pod etcd-master-node 

# Check etcd version
kubectl -n kube-system exec etcd-master-node -- sh -c "etcdctl version"

# 
kubectl -n kube-system exec etcd-master-node -- sh -c "ETCDCTL_API=3 etcdctl member list --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key"

# From the control plane node
sudo apt install etcd-client

# etcd backup and restore brief
export ETCDCTL_API=3  # needed to specify etcd api versions, not sure if it is needed anylonger with k8s 1.19+ 
etcdctl snapshot save -h   #show save options
etcdctl snapshot restore -h  #show restore options

# save the snapshot
sudo su -
export ETCDCTL_API=3 
export ENDPOINT=https://192.168.7.40:2379
sudo etcdctl snapshot save /tmp/new-snapshot.db --endpoints $ENDPOINT --cert=/etc/kubernetes/pki/etcd/server.crt  --key=/etc/kubernetes/pki/etcd/server.key --cacert=/etc/kubernetes/pki/etcd/ca.crt
exit

# Verify the snapshot
etcdctl --write-out=table snapshot status /tmp/new-snapshot.db

```
</p>
</details>

## etcd restore

### Restore a single instance of etcd
<details><summary>show</summary>
<p>

Do the restore to a new directory.

```
sudo ETCDCTL_API=3 etcdctl snapshot restore /tmp/new2-snapshot.db --name master-node --initial-cluster master-node=https://192.168.7.40:2380 --initial-advertise-peer-urls https://192.168.7.40:2380 --data-dir /var/lib/etcd-new

```

Now, to get the etcd pods to start using this new directory update the /etc/kubernetes/manifests/etcd.yaml configuration file with the new data folder, viz., /var/lib/etcd-new. The etcd pod(s) should automatically be restarted when saving the etcd.yaml manifest file. If not, try deleting the etcd pod(s). Or, restart of the kubelet on all control plane nodes.

The docs say, "Stop the kube-apiservers before performing the restore."

```
# Stop kube-apiserver
sudo mv /etc/kubernetes/manifests/kube-apiserver.yaml /tmp

# Change the data path to /var/lib/etcd-new
sudo vim /etc/kubernetes/manifests/etcd.yaml

# Start kube-apiserver
sudo mv tmp/kube-apiserver.yaml /etc/kubernetes/manifests/kube-apiserver.yaml
# To restart
kubectl delete pod/kube-apiserver-master-k8s -n kube-system

# Look to see that the pod is updated
kubectl describe pod -n kube-system etcd-master-node

```
See [openshift restore cluster state](https://docs.openshift.com/container-platform/4.4/backup_and_restore/disaster_recovery/scenario-2-restoring-cluster-state.html)

</p>
</details>