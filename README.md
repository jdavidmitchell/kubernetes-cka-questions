# kubernetes-cka-questions

1) Cluster Architecture, Installation & Configuration – 25%
2) Workloads & Scheduling – 15%
3) Services & Networking – 20%4) Storage – 10%
4) Storage – 10%
5) Troubleshooting – 30%

### Recommendations
- Use tmux to create more than one pane and switch between panes.
- Use an alias for kubectl: ```$ alias k=kubectl```
- Use hinting and completion for kubectl.


### https://github.com/stretchcloud/cka-lab-practice

