# Setting up kubeadm

[See docs](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)


### For the first time, install kubelet, kubeadm and kubectl (1.19.6-00 version)

<details><summary>show</summary>
<p>

```
sudo su -
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
exit

sudo apt-get update
sudo apt-get install -y kubelet=1.19.6-00 kubeadm=1.19.6-00 kubectl=1.19.6-00
sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl daemon-reload
sudo systemctl restart kubelet
```

The kubelet is now restarting every few seconds, as it waits in a crashloop for kubeadm to tell it what to do. So, one must run ```kubeadm --init``` for the kubelet to start. In the logs for kubelet, one will see that the kubelet configuration file, viz., /var/lib/kubelet/config.yaml, is missing, and therefore, kubelet will fail to start.

</p>
</details>

Check the status of kubelet and the logs, if there is a problem.
<details><summary>show</summary>
<p>

```
sudo systemctl status kubelet
journalctl -ue kubelet
journalctl -xue kubelet
```
</p>
</details>

### To reinstall or downgrade kubelet, kubeadm or kubectl, use the "apt-mark unhold" command

<details><summary>show</summary>
<p>

```
sudo apt-mark unhold kubelet kubeadm kubectl
sudo apt-get install kubelet=1.18.14-00 kubeadm=1.18.14-00 kubectl=1.18.14-00
sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl daemon-reload
sudo systemctl restart kubelet

```
</p>
</details>


### (Optional) Setup CLI word completion for kubectl and create an alias

<details><summary>show</summary>
<p>

```
source <(kubectl completion bash)
complete -F __start_kubectl k
alias k=kubectl

```
</p>
</details>
