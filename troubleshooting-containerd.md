# Troubleshooting containerd

[Troubleshooting kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/troubleshooting-kubeadm/)


## Check versions
<details><summary>show</summary>
<p>

```
containerd --version
ctr --version
# crictl is from cri-tools
crictl --version
runc --version

```
</p>
</details>

## Check Logs
<details><summary>show</summary>
<p>

```
sudo systemctl status containerd

# journalctl -ue kubelet
# journalctl -xue kubelet

```
</p>
</details>

## Is there a problem with containerd?

<details><summary>show</summary>
<p>

Check configuration of containerd: /etc/containerd/config.toml
```
# Does containerd have a configuration file?
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml

sudo containerd config dump

```
[Docs for runc runtimes under cri](https://github.com/containerd/cri/blob/master/docs/config.md)

</p>
</details>

Check the plugins
<details><summary>show</summary>
<p>

```
sudo ctr plugin ls

```
</p>
</details>

### Setup crictl
<details><summary>show</summary>
<p>

```
sudo su -
cat <<EOF | sudo tee /etc/crictl.yaml
runtime-endpoint: unix:///run/containerd/containerd.sock
image-endpoint: unix:///run/containerd/containerd.sock
timeout: 10
debug: true
EOF
exit

crictl info
# Can you pull an image?
crictl pull busybox

```
See [CRICTL User Guide](https://github.com/containerd/cri/blob/master/docs/crictl.md)

See [Container Runtime Interface (CRI) CLI](https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md)

</p>
</details>

### Install cgroup-tools
<details><summary>show</summary>
<p>

```
sudo apt install cgroup-tools
sudo lscgroup | grep kube

```
</p>
</details>

## kubeadm init: Cannot pull images

Look at the kubeadm defaults.
<details><summary>show</summary>
<p>

[go docs kubeadm v1beta1](https://godoc.org/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta1)
[go docs kubeadm v1beta2](https://godoc.org/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta2)

```
sudo kubeadm config print init-defaults

# Check criSocket
kind: InitConfiguration
nodeRegistration:
  criSocket:

```
</p>
</details>

Create a kubeadm configuration file.
<details><summary>show</summary>
<p>

```
cat <<EOF | tee containerd.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  criSocket: unix:///run/containerd/containerd.sock
  kubeletExtraArgs:
    cgroup-driver: "systemd"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "10.100.0.1/24" # --pod-network-cidr
EOF

```
</p>
</details>

### See if you can pull an image
<details><summary>show</summary>
<p>

```
sudo kubeadm config images pull --config containerd.yaml

```
</p>
</details>
