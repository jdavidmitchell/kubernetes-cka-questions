# Creating Highly Available clusters with kubeadm

[Manage a highly available Kubernetes cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/)

[Weaveworks Kubeadm HA cluster](https://www.weave.works/blog/running-highly-available-clusters-with-kubeadm)

## First steps

### Create a load balancer for kube-apiserver

The health check for an apiserver is a TCP check on the port the kube-apiserver listens on (default value :6443).

Make sure the address of the load balancer always matches the address of kubeadm's ControlPlaneEndpoint.

Test the load balancer.

<details><summary>show</summary>
<p>

```
nc -v <LOAD_BALANCER_IP> <PORT>

```
</p>
</details>

### Initialize the first control plane node

<details><summary>show</summary>
<p>

```
export LOAD_BALANCER_DNS="lb.default"
export LOAD_BALANCER_PORT=8080
sudo kubeadm init --control-plane-endpoint "$LOAD_BALANCER_DNS:$LOAD_BALANCER_PORT" --upload-certs

```
The --upload-certs flag is used to upload the certificates that should be shared across all the control-plane instances. When --upload-certs is used with kubeadm init, the certificates of the primary control plane are encrypted and uploaded in the kubeadm-certs Secret.

</p>
</details>

### Initialize the first control plane node with a config file

<details><summary>show</summary>
<p>

Change the InitConfiguration, add JoinConfiguration

```
sudo su -
vim containerd.yaml

apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  criSocket: unix:///run/containerd/containerd.sock
  kubeletExtraArgs:
    cgroup-driver: "systemd"
controlPlane:
  localAPIEndpoint:
    advertiseAddress: "<LOAD_BALANCER_IP/DNS>"
    bindPort: 6443
  certificateKey: "e6a2eb8381237ab72a4fa94f30285ec12a9694d750b9785706a83bfcbbbd2204"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: hjnu4i.ebza7otvg9axuqv7
    apiServerEndpoint: "k8s-8152541997.us-west-1.elb.amazonaws.com:6443"
    unsafeSkipCAVerification: true
nodeRegistration:
  name: ip-172-31-50-70.us-west-2.compute.internal
  kubeletExtraArgs:
    cloud-provider: aws
controlPlane:
  localAPIEndpoint:
    advertiseAddress: "<LOAD_BALANCER_IP>"
    bindPort: 6443
  certificateKey: "e6a2eb8381237ab72a4fa94f30285ec12a9694d750b9785706a83bfcbbbd2204"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "10.5.0.0/16"


exit

```

CertificateKey sets the key with which certificates and keys are encrypted prior to being uploaded in a secret in the cluster during the uploadcerts init phase.

[JoinConfiguration](https://github.com/kubernetes/kubernetes/issues/80582)

[add-new-control-plane-nodes-aws](https://blog.scottlowe.org/2019/04/16/using-kubeadm-add-new-control-plane-nodes-aws-integration/)

</p>
</details>

### Generate a custom --certificate-key

This key can be used during init, and it can later be used by join. The decryption key from --certificate-key expires after two hours, by default.

<details><summary>show</summary>
<p>

```
kubeadm certs certificate-key

```

</p>
</details>

### re-upload the certificates and generate a new decryption key

Use the following command on a control plane node that is already joined to the cluster:

<details><summary>show</summary>
<p>

```
sudo kubeadm init phase upload-certs --upload-certs

```

</p>
</details>

### Use Weave Net as the CNI

<details><summary>show</summary>
<p>

```
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

# Watch
kubectl get pod -n kube-system -w

```

</p>
</details>

### Set up other control plane nodes

Since kubeadm version 1.15 you can join multiple control-plane nodes in parallel.

<details><summary>show</summary>
<p>

```
sudo kubeadm join 192.168.0.200:6443 --token 9vr73a.a8uxyaju799qwdjv --discovery-token-ca-cert-hash sha256:7c2e69131a36ae2a042a339b33381c6d0d43887e2de83720eff5359e26aec866 --control-plane --certificate-key f8902e114ef118304e561c3ecd4d0b543adc226b7a07f675f56564185ffe0c07

```

The --control-plane flag tells kubeadm join to create a new control plane.
The --certificate-key ... will cause the control plane certificates to be downloaded from the kubeadm-certs Secret in the cluster and be decrypted using the given key.

</p>
</details>

### Set up worker nodes

<details><summary>show</summary>
<p>

```
sudo kubeadm join 192.168.0.200:6443 --token 9vr73a.a8uxyaju799qwdjv --discovery-token-ca-cert-hash sha256:7c2e69131a36ae2a042a339b33381c6d0d43887e2de83720eff5359e26aec866

```

</p>
</details>

## External etcd nodes

### Set up the etcd cluster

[setup-ha-etcd-with-kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/)

Copy the following files from any etcd node in the cluster to the first control plane node:

<details><summary>show</summary>
<p>

```
# the user@host of the first control-plane node
export CONTROL_PLANE="ubuntu@10.0.0.7"

scp /etc/kubernetes/pki/etcd/ca.crt "${CONTROL_PLANE}":
scp /etc/kubernetes/pki/apiserver-etcd-client.crt "${CONTROL_PLANE}":
scp /etc/kubernetes/pki/apiserver-etcd-client.key "${CONTROL_PLANE}":

```

</p>
</details>

### Set up the first control plane node

Create a file called kubeadm-config.yaml with the following contents:

<details><summary>show</summary>
<p>

```
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
kubernetesVersion: stable
controlPlaneEndpoint: "LOAD_BALANCER_DNS:LOAD_BALANCER_PORT"
etcd:
    external:
        endpoints:
        - https://ETCD_0_IP:2379
        - https://ETCD_1_IP:2379
        - https://ETCD_2_IP:2379
        caFile: /etc/kubernetes/pki/etcd/ca.crt
        certFile: /etc/kubernetes/pki/apiserver-etcd-client.crt
        keyFile: /etc/kubernetes/pki/apiserver-etcd-client.key

```

Then, run:
```
sudo kubeadm init --config kubeadm-config.yaml --upload-certs

# CNI Weave Net
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

```
</p>
</details>
