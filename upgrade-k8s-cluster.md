# Peform a version upgrade on Kubernetes cluster using kubeadm

[kubeadm upgrade](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)

[Reference: Peform a version upgrade on Kubernetes cluster using kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-upgrade/)


What version are you running?
<details><summary>show</summary>
<p>

```
kubeadm version

```
</p>
</details>

See what versions are available?
<details><summary>show</summary>
<p>

```
sudo apt update && apt-cache madison kubeadm
```
</p>
</details>

## First time install of kubeadm, kubelet and kubectl

The example below uses versions 1.18.14-00.
<details><summary>show</summary>
<p>

```
sudo su
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt
.kubernetes.io/ kubernetes-xenial main
EOF
exit

sudo apt-get update
sudo apt-get install -y kubelet=1.18.14-00 kubeadm=1.18.14-00 kubectl=1.18.14-00
sudo apt-mark hold kubelet kubeadm kubectl
```

The kubelet is now restarting every few seconds, as it waits in a crashloop for kubeadm to tell it what to do.

</p>
</details>

Check the status of kubelet and the logs, if there is a problem.
<details><summary>show</summary>
<p>

```
sudo systemctl status kubelet
journalctl -u kubelet
```
</p>
</details>


### Install version 1.18.14-00
<details><summary>show</summary>
<p>

```
sudo su -
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.18.14-00 && \
apt-mark hold kubeadm
exit

```
</p>
</details>

### Install version 1.19.6-00
<details><summary>show</summary>
<p>

```
sudo su -
apt-mark unhold kubeadm &&
apt-get update && apt-get install -y kubeadm=1.19.6-00 && \
apt-mark hold kubeadm
exit

```
</p>
</details>

## Version upgrade of kubeadm, kubelet and kubectl

### Upgrade from 1.18 to 1.19

#### Identify the version target
<details><summary>show</summary>
<p>

```
sudo apt update && apt-cache madison kubeadm

```
</p>
</details>

#### Upgrade kubeadm and plan it
<details><summary>show</summary>
<p>

```
apt-mark unhold kubeadm && \
apt-get install -y kubeadm=1.19.6-00 && \
apt-mark hold kubeadm

# Verify it works
kubeadm version

# Verify upgrade plan
kubeadm upgrade plan

```
</p>
</details>

#### Perform the upgrade on the first control plane node
<details><summary>show</summary>
<p>

```
# On the control plane
sudo kubeadm upgrade apply v1.19.6-00

# If you have a config file
sudo kubeadm upgrade apply v1.19.6-00 --config containerd.yaml

```
</p>
</details>

#### Upgrade the CNI plugin (calico)
<details><summary>show</summary>
<p>

TODO:
Apply new manifests
Apply/create RBAC rules
```

```
</p>
</details>

#### Perform the upgrade on the second and third control plane nodes
<details><summary>show</summary>
<p>

```
# sudo to the root user
sudo su -

# Do not use apply
kubeadm upgrade node v1.19.6-00

# If you have a config file
kubeadm upgrade node v1.19.6-00 --config containerd.yaml

# Mark node as un-schedulable and drain
kubectl drain <node-to-drain> --ignore-daemonsets

# Upgrade kubelet and kubectl
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.19.6-00 kubectl=1.19.6-00 && \
apt-mark hold kubelet kubectl

# Restart kubelet
systemctl daemon-reload
systemctl restart kubelet

# Mark node as schedulable
kubectl uncordon <node-to-drain>

# Exit from root user
exit

# Verify the status of the cluster
kubectl get nodes

```
</p>
</details>

#### Upgrade the worker nodes
<details><summary>show</summary>
<p>

```
sudo su -
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.19.6-00 && \
apt-mark hold kubeadm

# Verify version
kubeadm version

# upgrade
kubeadm upgrade node v1.19.6-00

# Mark node as un-schedulable and drain
kubectl drain <node-to-drain> --ignore-daemonsets

# Upgrade kubelet and kubectl
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.19.6-00 kubectl=1.19.6-00 && \
apt-mark hold kubelet kubectl

# Restart kubelet
systemctl daemon-reload
systemctl restart kubelet

# Mark node as schedulable
kubectl uncordon <node-to-drain>

# Verify the status of the cluster
kubectl get nodes

exit

```
</p>
</details>

#### Troubleshooting failure
<details><summary>show</summary>
<p>

```
# Run kubeadm upgrade again
sudo kubeadm upgrade node v1.19.6-00
# You can also run apply again with --force
sudo kubeadm upgrade apply v1.19.6-00 --force

# Backup folder
/etc/kubernetes/tmp

# Backup folders
kubeadm-backup-etcd-<date>-<time>
kubeadm-backup-manifests-<date>-<time>

# On failure of the auto rollback, it is possible roll back manually by copying etcd to:
/var/lib/etcd

# On failure of the auto rollback, it is possible roll back manually by copying the manifests to:
/etc/kubernetes/manifests

```
</p>
</details>