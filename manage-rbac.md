# Manage role based access control

[Manage role based access control](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)


## Enable RBAC
<details><summary>show</summary>
<p>

```
kube-apiserver --authorization-mode=Example,RBAC --other-options --more-options

```
</p>
</details>

## API objects

RBAC authorization uses the rbac.authorization.k8s.io API group. The RBAC API declares four kinds of Kubernetes object: Role, ClusterRole, RoleBinding and ClusterRoleBinding. An RBAC Role or ClusterRole contains rules that represent a set of permissions. Permissions are purely additive (there are no "deny" rules).

### Role & ClusterRole

A Role always sets permissions within a particular namespace. ClusterRole, by contrast, is a non-namespaced resource.

Create a Role in the "default" namespace that can be used to grant read access to pods:
<details><summary>show</summary>
<p>

```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: pod-reader
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["get", "watch", "list"]

```

</p>
</details>

Create a ClusterRole that can be used to grant read access to secrets in any particular namespace.

<details><summary>show</summary>
<p>

```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  # "namespace" omitted since ClusterRoles are not namespaced
  name: secret-reader
rules:
- apiGroups: [""]
  #
  # at the HTTP level, the name of the resource for accessing Secret
  # objects is "secrets"
  resources: ["secrets"]
  verbs: ["get", "watch", "list"]

```
</p>
</details>

### RoleBinding

Bind a role to a specific user. Grant the "pod-reader" Role to the user "jane" within the "default" namespace. This allows "jane" to read pods in the "default" namespace.
<details><summary>show</summary>
<p>

```
apiVersion: rbac.authorization.k8s.io/v1
# This role binding allows "jane" to read pods in the "default" namespace.
# You need to already have a Role named "pod-reader" in that namespace.
kind: RoleBinding
metadata:
  name: read-pods
  namespace: default
subjects:
# You can specify more than one "subject"
- kind: User
  name: jane # "name" is case sensitive
  apiGroup: rbac.authorization.k8s.io
roleRef:
  # "roleRef" specifies the binding to a Role / ClusterRole
  kind: Role #this must be Role or ClusterRole
  name: pod-reader # this must match the name of the Role or ClusterRole you wish to bind to
  apiGroup: rbac.authorization.k8s.io

```
</p>
</details>

### ClusterRoleBinding

Do a ClusterRoleBinding to allow any user in the group "manager" to read secrets in any namespace.
<details><summary>show</summary>
<p>

```
apiVersion: rbac.authorization.k8s.io/v1
# This cluster role binding allows anyone in the "manager" group to read secrets in any namespace.
kind: ClusterRoleBinding
metadata:
  name: read-secrets-global
subjects:
- kind: Group
  name: manager # Name is case sensitive
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: secret-reader
  apiGroup: rbac.authorization.k8s.io

```
</p>
</details>

### Test applying a manifest file of RBAC objects
<details><summary>show</summary>
<p>

```
kubectl auth reconcile -f my-rbac-rules.yaml --dry-run=client

# Apply
kubectl auth reconcile -f my-rbac-rules.yaml

# remove extra permissions and extra subjects
kubectl auth reconcile -f my-rbac-rules.yaml --remove-extra-subjects --remove-extra-permissions

```
</p>
</details>

### Grant a role to an application-specific service account
<details><summary>show</summary>
<p>

```
kubectl create rolebinding my-sa-view \
  --clusterrole=view \
  --serviceaccount=my-namespace:my-sa \
  --namespace=my-namespace

```
</p>
</details>


### Grant a role to the "default" service account in a namespace
<details><summary>show</summary>
<p>

If an application does not specify a serviceAccountName, it uses the "default" service account.

```
kubectl create rolebinding default-view \
  --clusterrole=view \
  --serviceaccount=my-namespace:default \
  --namespace=my-namespace

```
</p>
</details>

### Restricts its subject to only get or update a ConfigMap named my-configmap

<details><summary>show</summary>
<p>

```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: configmap-updater
rules:
- apiGroups: [""]
  #
  # at the HTTP level, the name of the resource for accessing ConfigMap
  # objects is "configmaps"
  resources: ["configmaps"]
  resourceNames: ["my-configmap"]
  verbs: ["update", "get"]

```
</p>
</details>

### To view the configuration of the default ClusterRole for the API discovery endpoint

<details><summary>show</summary>
<p>

```
kubectl get clusterroles system:discovery -o yaml

```
</p>
</details>

### kubectl create role

Create a Role named "pod-reader" that allows users to perform get, watch and list on pods
<details><summary>show</summary>
<p>

```
kubectl create role pod-reader --verb=get --verb=list --verb=watch --resource=pods

```
</p>
</details>

Create a Role named "pod-reader" with resourceNames specified
<details><summary>show</summary>
<p>

```
kubectl create role pod-reader --verb=get --resource=pods --resource-name=readablepod --resource-name=anotherpod

```
</p>
</details>

Create a Role named "foo" with apiGroups specified
<details><summary>show</summary>
<p>

```
kubectl create role foo --verb=get,list,watch --resource=replicasets.apps

```
</p>
</details>

Create a Role named "foo" with subresource permissions
<details><summary>show</summary>
<p>

```
kubectl create role foo --verb=get,list,watch --resource=pods,pods/status

```
</p>
</details>

Create a Role named "my-component-lease-holder" with permissions to get/update a resource with a specific name
<details><summary>show</summary>
<p>

```
kubectl create role my-component-lease-holder --verb=get,list,watch,update --resource=lease --resource-name=my-component

```
</p>
</details>

Create a ClusterRole named "pod-reader" that allows user to perform get, watch and list on pods
<details><summary>show</summary>
<p>

```
kubectl create clusterrole pod-reader --verb=get,list,watch --resource=pods

```
</p>
</details>

Create a ClusterRole named "pod-reader" with resourceNames specified
<details><summary>show</summary>
<p>

```
kubectl create clusterrole pod-reader --verb=get --resource=pods --resource-name=readablepod --resource-name=anotherpod

```
</p>
</details>

### kubectl create rolebinding

Within the namespace "acme", grant the permissions in the "admin" ClusterRole to a user named "bob" 
<details><summary>show</summary>
<p>

```
kubectl create rolebinding bob-admin-binding --clusterrole=admin --user=bob --namespace=acme

```
</p>
</details>

Within the namespace "acme", grant the permissions in the "view" ClusterRole to the service account in the namespace "acme" named "myapp"
<details><summary>show</summary>
<p>

```
kubectl create rolebinding myapp-view-binding --clusterrole=view --serviceaccount=acme:myapp --namespace=acme

```
</p>
</details>

Within the namespace "acme", grant the permissions in the "view" ClusterRole to a service account in the namespace "myappnamespace" named "myapp"
<details><summary>show</summary>
<p>

```
kubectl create rolebinding myappnamespace-myapp-view-binding --clusterrole=view --serviceaccount=myappnamespace:myapp --namespace=acme

```
</p>
</details>

### kubectl create clusterrolebinding

Across the entire cluster, grant the permissions in the "cluster-admin" ClusterRole to a user named "root"
<details><summary>show</summary>
<p>

```
kubectl create clusterrolebinding root-cluster-admin-binding --clusterrole=cluster-admin --user=root

```
</p>
</details>

Across the entire cluster, grant the permissions in the "system:node-proxier" ClusterRole to a user named "system:kube-proxy"
<details><summary>show</summary>
<p>

```
kubectl create clusterrolebinding kube-proxy-binding --clusterrole=system:node-proxier --user=system:kube-proxy

```
</p>
</details>

### kubectl auth reconcile

Test applying a manifest file of RBAC objects, displaying changes that would be made
<details><summary>show</summary>
<p>

```
kubectl auth reconcile -f my-rbac-rules.yaml --dry-run=client

```
</p>
</details>

## ServiceAccount permissions

Grant a role to an application-specific service account. Grant read-only permission within "my-namespace" to the "my-sa" service account
<details><summary>show</summary>
<p>

```
kubectl create rolebinding my-sa-view \
  --clusterrole=view \
  --serviceaccount=my-namespace:my-sa \
  --namespace=my-namespace

```
</p>
</details>


Grant a role to the "default" service account in a namespace. grant read-only permission within "my-namespace" to the "default" service account.
<details><summary>show</summary>
<p>

```
kubectl create rolebinding default-view \
  --clusterrole=view \
  --serviceaccount=my-namespace:default \
  --namespace=my-namespace

```
</p>
</details>