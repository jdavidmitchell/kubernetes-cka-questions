# Provision infrastructure (two node cluster)

[Provision underlying infrastructure to deploy Kubernetes cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

Get two instances ready to deploy kubernetes:
- master-node
- worker01

### (Optional) Install openssh server on your 2nd node (worker01)
 
```
sudo apt install ssh
ssh-keygen
```

### Assign Unique Hostname for Each Server Node

```
sudo hostnamectl set-hostname master-node
sudo hostnamectl set-hostname worker01

```
### (Optional) Add the ssh public key from master-node to authorized_keys on worker01

On master-node:
```
cat ~/.ssh/id_rsa.pub
```
On worker01, and the key above:
```
vim ~/.ssh/authorized_keys
```

### (Optional) Modify /etc/hosts on both nodes with the ips of node01 and node02
```
sudo vim /etc/hosts
```
For example:
```
192.168.7.95    master-node
192.168.7.40    worker01
```

### (Optional) Install tmux
```
sudo install tmux
```
Create a new pane with Ctl-b "
In one of the panes, ssh into worker01.
Syncronize panes with Ctl-b : setw synchronize-panes
Turn off syncronization with Ctl-b : setw synchronize-panes off


### Each node must have an ethernet interface and a unique product_uuid

<details><summary>show</summary>
<p>

```
ifconfig -a
sudo cat /sys/class/dmi/id/product_uuid
```
</p>
</details>

### Disable swap and make it permanent by editing /etc/fstab

<details><summary>show</summary>
<p>

```
swapon --show
sudo swapoff -a
sudo vim /etc/fstab

```
</p>
</details>

### Load modules: overlay and br_netfilter

<details><summary>show</summary>
<p>

```
sudo su -
cat > /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF
exit

sudo modprobe overlay
sudo modprobe br_netfilter

```
</p>
</details>

### Let iptables see bridged traffic

<details><summary>show</summary>
<p>

```
sudo su -
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
exit
sudo sysctl --system

```
</p>
</details>

## Kubernetes uses a container runtime. Either:
- containerd
- docker
- CRI-O

### Using containerd: Install containerd on all nodes (using Ubuntu 20.04)

See [Container runtimes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)

<details><summary>show</summary>
<p>

```
sudo apt-get update
sudo apt-get install -y containerd
# Configure containerd
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml

# To use the systemd cgroup driver in /etc/containerd/config.toml with runc, set:
sudo vim /etc/containerd/config.toml

[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
  ...
  [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true

# To use containerd, you will need to run kubeadm init with a configuration file (see below), e.g., kubeadm init --config containerd.yaml

sudo systemctl restart containerd
sudo systemctl status containerd

```
</p>
</details>

### Using containerd: Install containerd on all nodes (using Ubuntu 16.04)

See [Container runtimes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)

<details><summary>show</summary>
<p>

```
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"

## Install containerd version 1.3.9-1 (or latest version)
sudo apt-get update && sudo apt-get install -y containerd.io=1.3.9-1

# Configure containerd
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml

# To use the systemd cgroup driver in /etc/containerd/config.toml with runc, set:
sudo vim /etc/containerd/config.toml

[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
  ...
  [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true

# To use containerd, you will need to run kubeadm init with a configuration file (see below), e.g., kubeadm init --config containerd.yaml

# Restart containerd
sudo systemctl restart containerd
# Confirm version
containerd -v

```

</p>
</details>

### Using containerd: Run kubeadm with a configuration file

#### Take a look at the kubeadm defaults
<details><summary>show</summary>
<p>

```
sudo kubeadm config print init-defaults

```
</p>
</details>

#### Construct a containerd.yaml with the kubeadm settings.

<details><summary>show</summary>
<p>

When using kubeadm, manually configure [the cgroup driver for kubelet.](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#configure-cgroup-driver-used-by-kubelet-on-control-plane-node)

```
# sudo su -
cat <<EOF | tee containerd.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  criSocket: unix:///run/containerd/containerd.sock
  kubeletExtraArgs:
    cgroup-driver: "systemd"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "10.100.0.1/24" # --pod-network-cidr
EOF
# exit

# After installation of kubeadm, use it with the above config file.
sudo kubeadm init --config containerd.yaml

# If kubelet is running, restart
sudo systemctl daemon-reload
sudo systemctl restart kubelet

```

See [kubeadm init with config file](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#config-file)

One has to configure kubelet to use containerd rather than docker as the CRI.

One can test containerd by pulling the images with kubeadm. [How To Manually Pull Container images used by Kubernetes kubeadm](https://computingforgeeks.com/manually-pull-container-images-used-by-kubernetes-kubeadm/)

```
sudo kubeadm config images pull --config containerd.yaml
```
</p>
</details>

### Using containerd: Test containerd
[Manually Loading Container Images with containerd](https://blog.scottlowe.org/2020/01/25/manually-loading-container-images-with-containerd/)

[ctr docs](https://github.com/containerd/cri/blob/master/docs/crictl.md)

<details><summary>show</summary>
<p>

```
sudo ctr image ls
sudo ctr image pull k8s.gcr.io/kube-apiserver:v1.18.14
sudo ctr image pull docker.io/calico/node:v3.11.2
sudo ctr image ls -q
```

[CRICTL User Guide](https://github.com/containerd/cri/blob/master/docs/crictl.md)

Setup crictl.
```
sudo su -
cat <<EOF | sudo tee /etc/crictl.yaml
runtime-endpoint: unix:///run/containerd/containerd.sock
image-endpoint: unix:///run/containerd/containerd.sock
timeout: 10
debug: true
EOF
exit

crictl pull busybox
crictl info

```
</p>
</details>

### Remove containerd

<details><summary>show</summary>
<p>

```
# Purge
sudo apt-get purge -y containerd
# Remove runtime and configuration file.
sudo rm -fr /var/run/containerd/
sudo rm /etc/containerd/config.toml
```
</p>
</details>

### Using docker: Install docker on all nodes (using Ubuntu 20.04)

<details><summary>show</summary>
<p>

```
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"

sudo apt update

# It is not recommended that you install the latest version of docker
# sudo apt-get install docker-ce
# At the present time (December 2020), the latest recommended version of docker is 19.03.14
sudo apt install docker-ce=5:19.03.14~3-0~ubuntu-$(lsb_release -cs) \
docker-ce-cli=5:19.03.14~3-0~ubuntu-$(lsb_release -cs) \
containerd.io=1.2.13-2

# Set up the Docker daemon
sudo su -
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
exit

sudo systemctl start docker
sudo systemctl enable docker
sudo systemctl status docker

```

See [container runtimes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/) for docker versions.

</p>
</details>

### (Optional) add your user to the docker group

<details><summary>show</summary>
<p>

```
sudo groupadd docker
sudo usermod -aG docker [user]
su - [user]
id -nG
```
</p>
</details>

### Remove docker

<details><summary>show</summary>
<p>

```
# You can remove images with:
docker system prune -a
# Purge
sudo apt-get purge -y docker-ce docker-ce-cli
sudo apt-get autoremove -y --purge docker-ce docker-ce-cli
# For older versions of docker
sudo apt-get purge -y docker-engine docker docker.io
sudo apt-get autoremove -y --purge docker-engine docker docker.io 
# You can remove all traces with:
sudo rm -rf /var/lib/docker /etc/docker
sudo rm /etc/apparmor.d/docker
sudo groupdel docker
sudo rm -rf /var/run/docker.sock
```
</p>
</details>
