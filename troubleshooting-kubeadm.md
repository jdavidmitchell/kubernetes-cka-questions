# Troubleshooting kubeadm

[Troubleshooting kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/troubleshooting-kubeadm/)

## Start over :)
<details><summary>show</summary>
<p>

```
sudo kubeadm reset --force
rm -rf $HOME/.kube
docker system prune -af
docker image prune -af
```

</p>
</details>

## Cannot add a node to the cluster due to token expriation
<details><summary>show</summary>
<p>

Tokens expire after 24 hours.

```
kubeadm token list
kubeadm token create
```

If you don't have the value of --discovery-token-ca-cert-hash, you can get it by running the following command chain on the control-plane node:

```
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | \
   openssl dgst -sha256 -hex | sed 's/^.* //'
```
[create-cluster-kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

</p>
</details>

## node name duplicate, when joining a node to the cluster
<details><summary>show</summary>
<p>

In v1.18, kubeadm added prevention for joining a Node in the cluster if a Node with the same name already exists.

kubeadm join from v1.18 cannot join a cluster created by kubeadm v1.17.

Solution: Execute kubeadm init phase bootstrap-token on a control-plane node using kubeadm v1.18.
```
sudo kubeadm init phase bootstrap-token
```

</p>
</details>

## Executable not found (preflight error)
<details><summary>show</summary>
<p>

Solution: install it.
```
sudo apt install ebtables ethtool
```

</p>
</details>

## kubeadm waiting for control plane
```
[apiclient] Created API client, waiting for the control plane to become ready
```
<details><summary>show</summary>
<p>


Check:
- network connection
- the default cgroup driver for the kubelet is systemd, and docker is expecting cgroupfs
- control plane containers are crashlooping or hanging--look at the logs (e.g., docker logs or sudo ctr containers exec -a [container])
```
docker ps
sudo ctr containers
sudo ctr containers stats [container]
sudo ctr events
```

[containerd ctr docs](https://github.com/projectatomic/containerd/blob/master/docs/cli.md)

[manually pull images with kubeadm](https://computingforgeeks.com/manually-pull-container-images-used-by-kubernetes-kubeadm/)

</p>
</details>

## 'kubeadm reset' hangs on "Removing kubernetes-managed containers"
<details><summary>show</summary>
<p>

Solution: There is a problem with the docker service. Try restarting docker.
```
journalctl -ue docker
sudo systemctl restart docker.service
sudo kubeadm reset
```

</p>
</details>

## Pods in crashloop state
<details><summary>show</summary>
<p>


```
kubectl get pods --all-namespaces
```
Check:
- Pod Network: You might have to grant it more RBAC privileges or use a newer version.
- Docker version: If the Docker version is older than 1.12.1, remove the MountFlags=slave option.

</p>
</details>

## HostPort and HostIP (NodePort Service) functionality is not working
<details><summary>show</summary>
<p>


This is a problem with the pod network. ipv4 has to be used. This port-map plugin will forward traffic from one or more ports on the host to the container.

NodePort Service: Exposes the Service on each Node's IP at a static port (the NodePort). A ClusterIP Service, to which the NodePort Service routes, is automatically created. By default and for convenience, the Kubernetes control plane will allocate a port from a range (default: 30000-32767).

[Port-mapping plugin](https://www.cni.dev/plugins/meta/portmap/)

[Service Type NodePort](https://kubernetes.io/docs/concepts/services-networking/service/#nodeport)

</p>
</details>

## Pods are not accessible via their Service IP
<details><summary>show</summary>
<p>


Many network add-ons do not yet enable hairpin mode which allows pods to access themselves via their Service IP. ```hostname -i``` should return a routable IP address.

```
kubectl run -it --rm --restart=Never alpine --image=alpine sh
# Or a running pod
kubectl exec <POD-NAME> -c <CONTAINER-NAME> -- <COMMAND>

# From a Pod in the same Namespace
nslookup hostnames
# dig [hostname]
nslookup hostnames.default
nslookup hostnames.default.svc.cluster.local
cat /etc/resolv.conf
nslookup kubernetes.default

# From the controller
kubectl get service hostnames -o json
```

[Debugging Services](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/#a-pod-cannot-reach-itself-via-service-ip)

</p>
</details>

## x509: certificate signed by unknown authority
<details><summary>show</summary>
<p>

```
unset KUBECONFIG
export KUBECONFIG=/etc/kubernetes/admin.conf
# OR
mv  $HOME/.kube $HOME/.kube.bak
mkdir $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
</p>
</details>

## (On pod) getsockopt: no route to host
<details><summary>show</summary>
<p>


Check:
- Confirm that the IP can communicate with other IPs on the same subnet. Same subnet?
- Is the IP an internal IP? ``` ip addr show``` The workaround is to tell kubelet which IP to use using --node-ip (or extraargs) and restart the kubelet service.
```
systemctl daemon-reload
systemctl restart kubelet
```

</p>
</details>

## coredns pods have CrashLoopBackOff
<details><summary>show</summary>
<p>

Check:
- Upgrade to a newer version of Docker.
- Disable SELinux
- Modify the coredns deployment to set allowPrivilegeEscalation to true (can compromise security):
```
kubectl -n kube-system get deployment coredns -o yaml | \
  sed 's/allowPrivilegeEscalation: false/allowPrivilegeEscalation: true/g' | \
  kubectl apply -f -
```

</p>
</details>

## etcd pods restart continually (centos/fedora)
```
rpc error: code = 2 desc = oci runtime error: exec failed: container_linux.go:247: starting container process caused "process_linux.go:110: decoding init error from pipe caused \"read parent: connection reset by peer\""
```

<details><summary>show</summary>
<p>
CentOS 7 with Docker 1.13.1.84. Downgrade to Docker 1.13.1-75 or upgrade to docker-ce-18.06

```
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce-18.06.1.ce-3.el7.x86_64
```

</p>
</details>

## The NodeRegistration.Taints field is omitted (on init)
<details><summary>show</summary>
<p>


Remove the taint
```
kubectl taint nodes NODE_NAME node-role.kubernetes.io/master:NoSchedule-
```

</p>
</details>

## /usr is mounted read-only on nodes (Fedora)
<details><summary>show</summary>
<p>
Kubernetes components like the kubelet and kube-controller-manager use the default path of /usr/libexec/kubernetes/kubelet-plugins/volume/exec/, yet the flex-volume directory must be writeable for the feature to work. So, use a custom volume-plugin-dir in the configuration file of ```kubeadm init --config```

```
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  kubeletExtraArgs:
    volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
controllerManager:
  extraArgs:
    flex-volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
nodeRegistration:
  kubeletExtraArgs:
    volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"

```

</p>
</details>

## kubeadm upgrade plan prints out context deadline exceeded error message
<details><summary>show</summary>
<p>
This message is normal when running an external etcd. This issue is fixed as of version 1.19.

</p>
</details>

## kubeadm reset unmounts /var/lib/kubelet
<details><summary>show</summary>
<p>
Re-mount the /var/lib/kubelet directory after performing the kubeadm reset operation.

This is a regression introduced in kubeadm 1.15. The issue is fixed in 1.20.

</p>
</details>
