# Cluster Architecture, Installation and Configuration 25%

1. [Provision underlying infrastructure to deploy Kubernetes cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/) Go to: [provision-infrastructure.md](provision-infrastructure.md) & [setup-kubeadm.md](setup-kubeadm.md)
1. [Use kubeadm to install a basic cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/) See below
1. [Peform a version upgrade on Kubernetes cluster using kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-upgrade/) Go to: [upgrade-k8s-cluster.md](upgrade-k8s-cluster.md)
1. [Manage a highly available Kubernetes cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/)
   [Weaveworks Kubeadm HA cluster](https://www.weave.works/blog/running-highly-available-clusters-with-kubeadm) Go to: [create-ha-clusters.md](create-ha-clusters.md)
1. [Implement etcd backup and restore](https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster) Go to: [etcd-backup-restore.md](etcd-backup-restore.md)
1. [Manage role based access control](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) Go to: [manage-rbac.md](manage-rbac.md)


### Use kubeadm to create a two-node cluster.

<details><summary>show</summary>
<p>

```
kubeadm config print init-defaults
```

#### containerd install: Create a init.yaml file for kubeadm --config init.yaml

[go docs kubeadm](https://godoc.org/k8s.io/kubernetes/cmd/kubeadm)

[go docs kubeadm v1beta1](https://godoc.org/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta1)

[go docs kubeadm v1beta2](https://godoc.org/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta2)

```
cat <<EOF | tee containerd.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  criSocket: unix:///run/containerd/containerd.sock
  kubeletExtraArgs:
    cgroup-driver: "systemd"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "10.5.0.0/16" # --pod-network-cidr
EOF

kubeadm init --config containerd.yaml
```

Output using docker 20.10.1 (latest supported version of docker is 19.03):

```
kubeadm init

[WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". 
Please follow the guide at https://kubernetes.io/docs/setup/cri/                                                              

[WARNING SystemVerification]: this Docker version is not on the list of validated versions: 20.10.1. Latest validated 
version: 19.03

[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'                                 
[certs] Using certificateDir folder "/etc/kubernetes/pki"                                                                     
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [kubernetes kubernetes.default kubernetes.default.svc kubernetes.defaul
t.svc.cluster.local pop-os] and IPs [10.96.0.1 192.168.7.40]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate a[88/1226]
[certs] Generating "etcd/ca" certificate and key              
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [loca
lhost pop-os] and IPs [192.168.7.40 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [localh
ost pop-os] and IPs [192.168.7.40 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificat[78/1226]
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[kubelet-start] Writing kubelet environment file with flags to
 file "/var/lib/kubelet/kubeadm-flags.env"
 [kubelet-start] Writing kubelet configuration to file[69/1226]
b/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[control-plane] Using manifest folder "/etc/kubernetes/manifes
ts"            
[control-plane] Creating static Pod manifest for "kube-apiserv
er"            
[control-plane] Creating static Pod manifest for "kube-control
ler-manager"   
[control-plane] Creating static Pod manifest for "kub[60/1226]
er"            
[etcd] Creating static Pod manifest for local etcd in "/etc/ku
bernetes/manifests"            
[wait-control-plane] Waiting for the kubelet to boot up the co
ntrol plane as static Pods from directory "/etc/kubernetes/man
ifests". This can take up to 4m0s                             
[apiclient] All control plane components are healthy after 13.
503584 seconds 
[upload-config] Storing the configuration used in Con[51/1226]
ubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.20" in namesp
ace kube-system with the configuration for the kubelets in the
 cluster       
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node pop-os as control-plane 
by adding the labels "node-role.kubernetes.io/master=''" and "
node-role.kubernetes.io/control-plane='' (deprecated)"
[mark-control-plane] Marking the node pop-os as contr[42/1226]
by adding the taints [node-role.kubernetes.io/master:NoSchedul
e]             
[bootstrap-token] Using token: 28srgi.ryvxizcsmp76493e
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles           
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrp tokens to post CSRs in order for nodes to get long term certificate credentials 
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token                                                        
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster       
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to 
point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:
  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:  
  https://kubernetes.io/docs/concepts/cluster-administration/a
ddons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.7.40:6443 --token 28srgi.ryvxizcsmp76493e
 \             
    --discovery-token-ca-cert-hash sha256:49022fb4b4ef8df732d9
f3ba0d1edc2c516c5a61caaaf4d40b9d34d15b728d28
```
</p>
</details>

### Setup your pod network by installing calico
<details><summary>show</summary>
<p>

```
curl https://docs.projectcalico.org/manifests/calico.yaml -O
# Policy: Calico, IPAM: Calico, CNI: Calico, Overlay: IPIP, Routing: BGP, Datastore: Kubernetes
kubectl apply -f calico.yaml
```

[Setup your pod network](https://medium.com/@swapnasagarpradhan/install-a-kubernetes-cluster-on-rhel8-with-conatinerd-b48b9257877a#6434)

</p>
</details>

### Setup a pod network using kube-router

<details><summary>show</summary>
<p>

```
curl https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml -O
kubectl apply -f kubeadm-kuberouter.yaml
```
</p>
</details>

### Create an nginx deployment

<details><summary>show</summary>
<p>

```
curl https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/application/nginx-app.yaml -O
kubectl apply -f nginx-app.yaml
 ```

</p>
</details>