# Clean up after creating kind clusters

```
kubectl config unset users.kind-c01
kubectl config unset users.kind-c02
kubectl config unset contexts.kind-c01
kubectl config unset contexts.kind-c02
kubectl config unset clusters.kind-c01
kubectl config unset clusters.kind-c02
kubectl config get-contexts
```

Remove config file:
```
> ~/.kube/config
```

## Check for running docker containers and purge them

```
docker container ls -aq
docker container stop $(docker container ls -aq)
```

[Ref for kubectl](https://stackoverflow.com/questions/37016546/how-do-i-delete-clusters-and-contexts-from-kubectl-config)

[Ref for docker](https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/)